# install vips on mac:
```brew install vips```

# convert image using following command:
```vips dzsave input.[ext] myzoom --layout zoomify```